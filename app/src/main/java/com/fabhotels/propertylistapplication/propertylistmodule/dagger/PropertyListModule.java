package com.fabhotels.propertylistapplication.propertylistmodule.dagger;


import com.fabhotels.propertylistapplication.activity.PropertyListActivity;
import com.fabhotels.propertylistapplication.propertylistmodule.api.PropertyListApi;
import com.fabhotels.propertylistapplication.propertylistmodule.core.PropertyListPresenter;
import com.fabhotels.propertylistapplication.propertylistmodule.core.PropertyListView;
import com.fabhotels.propertylistapplication.propertylistmodule.core.PropertyModule;
import com.fabhotels.propertylistapplication.realm.RealmService;
import com.fabhotels.propertylistapplication.rx.RxSchedulers;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Niharika on 24-05-2018.
 * PropertyListModule
 */
@Module
public class PropertyListModule {

    private PropertyListActivity mPropertyActivityContext;

    public PropertyListModule(PropertyListActivity context) {
        this.mPropertyActivityContext = context;
    }


    @PropertyListScope
    @Provides
    PropertyListView provideView() {
        return new PropertyListView(mPropertyActivityContext);
    }

    @PropertyListScope
    @Provides
    PropertyListPresenter providePresenter(RxSchedulers schedulers, PropertyListView view, PropertyListApi api, PropertyModule propertyModule) {
        return new PropertyListPresenter(schedulers, api, view, propertyModule);
    }

    @PropertyListScope
    @Provides
    PropertyListActivity provideContext() {
        return mPropertyActivityContext;
    }


    @PropertyListScope
    @Provides
    PropertyModule provideModel(PropertyListApi api, RealmService realmService) {
        return new PropertyModule(mPropertyActivityContext, api, realmService);
    }
}
