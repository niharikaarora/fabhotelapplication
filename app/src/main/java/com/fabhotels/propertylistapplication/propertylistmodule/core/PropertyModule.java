package com.fabhotels.propertylistapplication.propertylistmodule.core;


import com.fabhotels.propertylistapplication.activity.PropertyListActivity;
import com.fabhotels.propertylistapplication.model.PropertyListItem;
import com.fabhotels.propertylistapplication.model.PropertyListResponse;
import com.fabhotels.propertylistapplication.propertylistmodule.api.PropertyListApi;
import com.fabhotels.propertylistapplication.realm.RealmService;
import com.fabhotels.propertylistapplication.utils.AppUtils;

import java.util.List;

import rx.Observable;

/**
 * Created by Niharika on 24-05-2018.
 * PropertyModule
 */

public class PropertyModule {

    private PropertyListActivity context;
    private PropertyListApi api;
    private RealmService realmService;

    public PropertyModule(PropertyListActivity context, PropertyListApi api, RealmService realmService) {
        this.api = api;
        this.context = context;
        this.realmService = realmService;

    }


    void writeData(List<PropertyListItem> propertyListItems) {
        realmService.writeData(propertyListItems);
    }

    List<PropertyListItem> readData() {
        return realmService.getPropertyList();
    }

    Observable<PropertyListResponse> provideListProperty() {
        return api.getPropertyList();
    }

    Observable<Boolean> isNetworkAvailable() {
        return Observable.just(AppUtils.isNetworkAvailable(context));
    }
}
