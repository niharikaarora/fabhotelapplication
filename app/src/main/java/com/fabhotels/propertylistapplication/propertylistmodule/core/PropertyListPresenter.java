package com.fabhotels.propertylistapplication.propertylistmodule.core;

import com.fabhotels.propertylistapplication.model.PropertyListResponse;
import com.fabhotels.propertylistapplication.propertylistmodule.api.PropertyListApi;
import com.fabhotels.propertylistapplication.rx.RxSchedulers;

import rx.Observer;

/**
 * Created by Niharika on 24-05-2018.
 * PropertyListPresenter
 */

public class PropertyListPresenter {

    private PropertyListApi api;
    private RxSchedulers rxSchedulers;
    private PropertyModule propertyModule;
    private PropertyListView mPropertyListView;

    public PropertyListPresenter(RxSchedulers schedulers, PropertyListApi api, PropertyListView propertyListView, PropertyModule propertyModule) {
        this.rxSchedulers = schedulers;
        this.mPropertyListView = propertyListView;
        this.api = api;
        this.propertyModule = propertyModule;

    }


    public void loadPropertyList() {
        mPropertyListView.showWait();
        api.getPropertyList()
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.androidThread())
                .unsubscribeOn(rxSchedulers.io())
                .subscribe(new Observer<PropertyListResponse>() {
                    @Override
                    public void onCompleted() {
                        mPropertyListView.removeWait();

                    }

                    @Override
                    public void onError(Throwable e) {
                        mPropertyListView.swapAdapter(propertyModule.readData());
                    }

                    @Override
                    public void onNext(PropertyListResponse propertyListItems) {
                        propertyModule.writeData(propertyListItems.getPropertyListing());
                        mPropertyListView.swapAdapter(propertyModule.readData());
                    }
                });
    }
}
